<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <!-- Indicators -->
  <ol class="carousel-indicators">
    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
    <li data-target="#myCarousel" data-slide-to="1"></li>
    <li data-target="#myCarousel" data-slide-to="2"></li>
  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner">
    <div class="item active">
      <img src="https://www.obiyaninfotech.com/wp-content/uploads/2022/06/Website-Development.jpg" alt="Banner1">
    </div>

    <div class="item">
      <img src="https://media.publit.io/file/w_1920,h_864,c_fit,q_80/Blogweb/2s_blog_04_web-dev-maintain.jpg" alt="banner2">
    </div>
    <div class="item">
      <img src="https://images.squarespace-cdn.com/content/v1/54c2a742e4b043776a0b0a67/1424286709206-LOXLTEDCOENB87G1E0AW/wdr-inside-banner-web-design.jpg?format=1500w" alt="banner3">
    </div>
    <div class="item">
      <img src="https://st3.depositphotos.com/29384342/35125/v/450/depositphotos_351255620-stock-illustration-abstract-purple-hexagon-banner-background.jpg" alt="banner4">
    </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#myCarousel" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right"></span>
    <span class="sr-only">Next</span>
  </a>
</div>
